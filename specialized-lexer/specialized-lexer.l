%option noyywrap
%x IFILE STRING STRINGSINGLE

%{
	struct bufstack {
		struct bufstack *prev; // previous entry
		YY_BUFFER_STATE bs; // saved buffer
		int lineno; // save line number
		char *filename; // name of this file
		FILE *f; // current file
		int indent; // current file indent
	} *curbs = 0;

	char *curfilename; // name of current input file

	int newfile(char *fn);
	int popfile(void);
	int calculateIndent(char *c);

	int indent;

	char* path;

%}

%%

" "*"#".*\n          { yylineno++; }
.*"!include"[ \t]*   { indent = indent + calculateIndent(yytext); BEGIN IFILE; }
\"                   { ECHO; BEGIN STRING; }
\'                   { ECHO; BEGIN STRINGSINGLE; }

<STRING>\"           { ECHO; BEGIN INITIAL; }
<STRING>.+           { ECHO; BEGIN INITIAL; }
<STRINGSINGLE>\'     { ECHO; BEGIN INITIAL; }
<STRINGSINGLE>.+     { ECHO; BEGIN INITIAL; }

<IFILE>[^ \t\n\">]+  {
			{ int c;
			  while((c = input()) && c != '\n');
			}
			yylineno++;
			if (!newfile(yytext))
			  yyterminate(); // no such file
			BEGIN INITIAL;
		     }

<IFILE>.|\n 	     {  fprintf(stderr, "%4d bad include line\n", yylineno);
			yyterminate();
		     }
<<EOF>>              { if (!popfile()) yyterminate(); printf("\n"); int i; for (i = 0; i < indent; i++) { printf(" "); } }

^.                   { fprintf(yyout, "%s", yytext); }
^\n                  { yylineno++; fprintf(yyout, "%s", yytext); }
\n                   { yylineno++; ECHO; int i; for (i = 0; i < indent; i++) { printf(" "); } }
.                    { ECHO; }

%%

int main(int argc, char **argv) {
	if (argc < 2) {
		fprintf(stderr, "need filename\n");
		return 1;
	}
	if (argc < 3) {
		fprintf(stderr, "need path\n");
		return 2;
	}
	path = argv[2];
	if (newfile(argv[1]))
		yylex();
}

int newfile(char *fn) {
	char *path_fn;
	int n;

	n = snprintf(NULL, 0, "%s%s", path, fn);

	path_fn = malloc(n + 1);
	snprintf(path_fn, n + 1, "%s%s", path, fn);

	FILE *f = fopen(path_fn, "r");
	struct bufstack *bs = malloc(sizeof(struct bufstack));
	
	// die if no file or no room
	if (!f) { perror(fn); return 0; }
	if (!bs) { perror("malloc"); exit(1); }

	// remember state
	if (curbs) curbs->lineno = yylineno;
	bs->prev = curbs;

	// set up current entry
	bs->bs = yy_create_buffer(f, YY_BUF_SIZE);
	bs->f = f;
	bs->filename = fn;
	bs->indent = indent;
	yy_switch_to_buffer(bs->bs);
	curbs = bs;
	yylineno = 1;
	curfilename = fn;

	free(path_fn);

	return 1;
}

int popfile(void) {
	struct bufstack *bs = curbs;
	struct bufstack *prevbs;

	if (!bs) return 0;

	// get rid of current entry
	fclose(bs->f);
	yy_delete_buffer(bs->bs);

	// switch back to previous
	prevbs = bs->prev;
	free(bs);

	if (!prevbs) return 0;

	yy_switch_to_buffer(prevbs->bs);
	curbs = prevbs;
	yylineno = curbs->lineno;
	curfilename = curbs->filename;
	indent = curbs->indent;
	return 1;
}

int calculateIndent(char *c) {
	int index;
	for (index = 0; c[index] == ' '; index++) {
		printf(" ");
	}
	
	int tempIndex;
	for (tempIndex = index; c[tempIndex] != ' '; tempIndex++) {
		printf("%c", c[tempIndex]);
	}
	printf("\n");
	int i;
	for (i = 0; i < indent + index + 1; i++) {
		printf(" ");
	}
	return index + 1;
}
