/*
 *Declarations for HASS
 */

/* Interface to the lexer */
extern int yylineno;
void yyerror(char *s, ...);


struct ast {
	int nodetype; // A, B --- A = ConnectorObject, B = ConnectorList
	struct ast* l;
	struct ast* r;
};

struct ast* newast(int type, struct ast *l, struct ast *r) {
	struct ast *a = malloc(sizeof(struct ast));
	a->nodetype = type;
	a->l = l;
	a->r = r;
	return a;	
};

struct identifier {
	int nodetype; // D
	char* str;
};

struct ast* newidentifier(char* str) {
	struct identifier *a = malloc(sizeof(struct identifier));
	a->nodetype = 'D';
	a->str = str;
	return (struct ast*)a;
};

struct string {
	int nodetype; // S
	char* str;
};

struct ast* newstring(char* str) {
	struct string *a = malloc(sizeof(struct string));
	a->nodetype = 'S';
	a->str = str;
	return (struct ast*)a;
};

struct integer {
	int nodetype; // I
	int i;
};

struct ast* newinteger(int i) {
	struct integer *a = malloc(sizeof(struct integer));
	a->nodetype = 'I';
	a->i = i;
	return (struct ast*)a;
};

struct real {
	int nodetype; // R
	double d;
};

struct ast* newreal(double d) {
	struct real *a = malloc(sizeof(struct real));
	a->nodetype = 'R';
	a->d = d;
	return (struct ast*)a;
};

struct simpleobject {
	int nodetype; // M
	char* str;
};

struct ast* newsimpleobject(char* str) {
	struct simpleobject *a = malloc(sizeof(struct simpleobject));
	a->nodetype = 'M';
	a->str = str;
	return (struct ast*)a;
};

struct fieldobject {
	int nodetype; // F
	char* str;
	struct ast* identifier;
};

struct ast* newfieldobject(char* str, struct ast* identifier) {
	struct fieldobject *a = malloc(sizeof(struct fieldobject));
	a->nodetype = 'F';
	a->str = str;
	a->identifier = identifier;
	return (struct ast*)a;
};

struct listobject {
	int nodetype; // L
	char* str;
	struct ast* list;
};

struct ast* newlistobject(char* str, struct ast* list) {
	struct listobject *a = malloc(sizeof(struct listobject));
	a->nodetype = 'L';
	a->str = str;
	a->list = list;
	return (struct ast*)a;
};

struct nobject {
	int nodetype; // N
	char* str;
	struct ast* object;
};

struct ast* newnobject(char* str, struct ast* object) {
	struct nobject *a = malloc(sizeof(struct nobject));
	a->nodetype = 'N';
	a->str = str;
	a->object = object;
	return (struct ast*)a;
};

struct elistobject {
	int nodetype; // E
	char* str;
};

struct ast* newelistobject(char* str) {
	struct elistobject *a = malloc(sizeof(struct elistobject));
	a->nodetype = 'E';
	a->str = str;
	return (struct ast*)a;
};

struct eobjectobject {
	int nodetype; // O
	char* str;
};

struct ast* neweobjectobject(char* str) {
	struct eobjectobject *a = malloc(sizeof(struct eobjectobject));
	a->nodetype = 'O';
	a->str = str;
	return (struct ast*)a;
};

struct simplelist {
	int nodetype; // P
	struct ast* identifier;
};

struct ast* newsimplelist(struct ast* identifier) {
	struct simplelist *a = malloc(sizeof(struct simplelist));
	a->nodetype = 'P';
	a->identifier = identifier;
	return (struct ast*)a;
};

struct objectlist {
	int nodetype; // J
	struct ast* object;
};

struct ast* newobjectlist(struct ast* object) {
	struct objectlist *a = malloc(sizeof(struct objectlist));
	a->nodetype = 'J';
	a->object = object;
	return (struct ast*)a;
};


/* Nodes in the abstract syntax tree */
/*
struct ast {
	int nodetype; // type C for connector 
	int subtype; 
	// subtype general: A = ConnectorObject, B = ConnectorList 
	struct ast *l;
	struct ast *r;
	// subtypes:
	// F = FieldObject, L = ListObject, N = NObject,
	// O = ObjectList
	//
};

struct identifierval {
	int nodetype; // type I for identifier 
	int subtype;
	char* string; // subtype S for string, D for identifier, C for inClude 
	double dnumber; // substype R for real 
	int inumber; // subtype I for integer 
};

struct objectval {
	int nodetype; // type O for object 
	int subtype;
	char* str;
	// subtypes: 
	// S = SimpleObject, F = FieldObject, L = ListObject,
	// N = NObject, E = ElistObject, O = EObjectObject,
	//
	// X = ObjectList
	//
};	

struct listval {
	int nodetype; // type L for list 
	int subtype;
	// subtypes:
	// S = SimpleList, O = ObjectList 
	//
};

struct unkval {
	int nodetype; // type U for unknown 
};
*/

// Helpers 
char* concat(const char*, const char*);
char* wrapInQuotes(const char*);

// Build an AST 
/*
struct ast *newast(int subtype, struct ast *l, struct ast *r);
struct ast *newstr(char* str);
struct ast *newreal(double d);
struct ast *newint(int i);
struct ast *newlst(int subtype);
struct ast *newobj(int subtype, char* str);
*/

// Evaluate an AST 
char* eval(struct ast*);
char* evalConnector(struct ast*);
char* evalIdentifier(struct ast*);
char* evalObject(struct ast*);
char* evalList(struct ast*);

// Delete and free an AST 
void treefree(struct ast*);

