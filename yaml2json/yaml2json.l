%option noyywrap
%option yylineno

%x STRING SINGLEQUOTE MAIN DUMMY

%{
#include <stdio.h>
#define YY_DECL int yylex()
#include "yaml2json.tab.h"
#include "stack.h"

//#define PREVIOUS_INDENTS (struct Stack*) createStack(20)
char *str = "";
int currentIndent = 0;
struct Stack* PREVIOUS_INDENTS;
int seenDash = 0; // 1 = true, 0 = false
int debugFlag = 0;


char* concat(const char *s1, const char *s2) {
	char *result = malloc(strlen(s1)+strlen(s2) + 1); // +1 for the null-terminator
	// TODO - in real code you would check for errors in malloc here

	strcpy(result, s1);
	strcat(result, s2);
	return result;
}

char* cp(const char* s) {
	char *result = malloc(strlen(s) + 1);
	strcpy(result, s);
	return result;
}

void debug(char* state, char* rule, char* text) {
	if (debugFlag) {
		printf("State: %s, Rule: %s Text: %s\n", state, rule, text);	
	}
}

void trim(char* c) {
	memmove(c, c + 1, strlen(c) - 1);
	c[strlen(c) - 2] = '\0';
}

void keytrim(char* c) {
	int i;
	for (i = strlen(c) - 1; i > 0; i--) {
		
		if (c[i] == ' ' && c[i - 1] == ':') {
			memmove(c, c, i - 2);
			c[i - 1] = '\0';
			break;
		}
		if (c[i] == '\n' && c[i - 1] == ':') {
			memmove(c, c, i - 2);
			c[i - 1] = '\0';
			break;
		}		
	}
}

void newline() {
	yylineno++;
	currentIndent = 0;
}

%}


DIGITS 		[0-9]+
REAL            [0-9]+\.[0-9]+
KEYCHAR         [a-zA-Z0-9_]
IDCHAR          [a-zA-Z0-9_/.]
QUOTED          [a-zA-Z0-9_/.: ]
WS              [ \t]
SINGLEQUOTE	([-a-zA-Z0-9$_/:\"*]|\\[^.])       
STRINGDOUBLE	([-a-zA-Z0-9$_/:\'*]|\\[^.])       

%%

<INITIAL,MAIN,STRING,SINGLEQUOTE><<EOF>>   { 	debug("INITIAL", "EOF", "");
						pop(PREVIOUS_INDENTS);
						if (isEmpty(PREVIOUS_INDENTS)) {
							debug("INITIAL", "EOF", "EOF");
							return 0;
						}
						debug("INITIAL", "EOF", "DEDENT");
						unput('\n');
						return T_DEDENT;
	                                    }
<MAIN>\n                                 {debug("MAIN", "NEWLINE", yytext); BEGIN INITIAL; newline(); } 
<MAIN>\'{SINGLEQUOTE}+\'{WS}*":"{WS}+   {debug("MAIN", "Q KEY WS", yytext); keytrim(yytext); trim(yytext); yylval.sval = cp(yytext); return T_KEY;}
<MAIN>\'{SINGLEQUOTE}+\'{WS}*":"{WS}*\n {debug("MAIN", "Q KEY NEWLINE", yytext); newline(); BEGIN INITIAL; keytrim(yytext); trim(yytext); yylval.sval = cp(yytext); return T_KEY;}
<MAIN>\"{STRINGDOUBLE}+\"{WS}*":"{WS}+   {debug("MAIN", "Q KEY WS", yytext); keytrim(yytext); trim(yytext); yylval.sval = cp(yytext); return T_KEY;}
<MAIN>\"{STRINGDOUBLE}+\"{WS}*":"{WS}*\n {debug("MAIN", "Q KEY NEWLINE", yytext); newline(); BEGIN INITIAL; keytrim(yytext); trim(yytext); yylval.sval = cp(yytext); return T_KEY;}
<MAIN>{KEYCHAR}+{WS}*":"{WS}+            {debug("MAIN", "KEY WS", yytext); keytrim(yytext); yylval.sval = cp(yytext); return T_KEY;}
<MAIN>{KEYCHAR}+{WS}*":"\n               {debug("MAIN", "KEY NEWLINE", yytext); newline(); keytrim(yytext); BEGIN INITIAL; yylval.sval = cp(yytext); return T_KEY;}
<MAIN>"-"                                {debug("MAIN", "DASH", yytext); return T_DASH;}
<MAIN>"["                                {debug("MAIN", "LSPARAN", yytext); return T_LSPARAN;}
<MAIN>"]"                                {debug("MAIN", "RSPARAN", yytext); return T_RSPARAN;}
<MAIN>"{"                                {debug("MAIN", "LCPARAN", yytext); return T_LCPARAN;}
<MAIN>"}"                                {debug("MAIN", "RCPARAN", yytext); return T_RCPARAN;}
<MAIN>","                                {debug("MAIN", "COMMA", yytext); return T_COMMA;}
<MAIN>\"                                 {debug("MAIN", "STRING", yytext); str = ""; BEGIN STRING;}
<MAIN>\'                                 {debug("MAIN", "SINGLEQUOTE", yytext); str = ""; BEGIN SINGLEQUOTE;}
<MAIN>-?{DIGITS}                           {debug("MAIN", "INT", yytext); yylval.ival = atoi(yytext); return T_INT;}
<MAIN>-?{REAL}                             {debug("MAIN", "REAL", yytext); yylval.fval = atof(yytext); return T_REAL;}
<MAIN>({IDCHAR}+|":")*{IDCHAR}+          {debug("MAIN", "ID", yytext); yylval.sval = cp(yytext); return T_ID;}
<MAIN>{WS}+                              {/*debug("MAIN", "WS", yytext);*/}
<MAIN>"x: x: x:"                         {debug("MAIN", "END", yytext); return T_END;}
                             
<STRING>\"                         { debug("STRING", "QUOTE", ""); BEGIN MAIN; yylval.sval = cp(str); return T_STRING; }
<SINGLEQUOTE>\'                    { debug("SINGLEQUOTE", "SINGLEQUOTE", ""); BEGIN MAIN; yylval.sval = cp(str); return T_STRING; }
<SINGLEQUOTE>\"			   { debug("SINGLEQUOTE", "QUOTE", "" ); str = concat(str, "'"); }
<STRING>\\\"                       { debug("STRING", "ESCAPED QUOTE", ""); str = concat(str, "'"); }
<SINGLEQUOTE>\\\'                  { debug("SINGLEQUOTE", "ESCAPED SINGLEQUOTE", ""); str = concat(str, "'"); }
<STRING,SINGLEQUOTE>.              { debug("STRINGS", "CHAR", yytext); str = concat(str, yytext); } 

<DUMMY>.                            {printf("%s\n", yytext); }
<DUMMY>\n                           {printf("%s\n", yytext); }


<INITIAL>\n                         {/*debug("INITIAL", "NEWLINE", yytext);*/ yylineno++;}
<INITIAL>{WS}+\n                    {/*debug("INITIAL", "WS NEWLINE", yytext);*/ yylineno++;}
<INITIAL>{WS}*"#".*                 {/*debug("INITIAL", "COMMENT", yytext)*/;}
<INITIAL>{WS}+                      {/*debug("INITIAL", "WS+", yytext);*/ currentIndent = strlen(yytext);}
<INITIAL>{WS}*"- "                  {debug("INITIAL", "DASH", yytext); currentIndent = strlen(yytext); seenDash++;}

<INITIAL>""/.                       {/*	debug("INITIAL", "REC", yytext);*/
					
					if (currentIndent > peek(PREVIOUS_INDENTS)) {
						debug("INITIAL", "INDENT", yytext);
						push(PREVIOUS_INDENTS, currentIndent);	
						return T_INDENT;
					}
					if (currentIndent < peek(PREVIOUS_INDENTS)) {
						debug("INITIAL", "DEDENT", yytext);
						pop(PREVIOUS_INDENTS);
						return T_DEDENT;
					}
					if (seenDash) {
						debug("INITIAL", "DASH", yytext);
						BEGIN MAIN;
						seenDash--;
						return T_DASH;
					}
					
					debug("INITIAL", "SAME", yytext);
					BEGIN MAIN;
					return T_SAME;
				    ;}


%%
/*

YYSTYPE yylval;

#include <stdarg.h>

int main(int argc, char** av) {
	PREVIOUS_INDENTS = createStack(20);
	int token;
	yyin = fopen(av[1], "r");
	//printf("%d", yylex());
	//printf("%d", yylex());
	int counter = 0;
	do {
		counter++;
		token = yylex();
		printf("Token: %d (%s)\n", token, yytext);
	} while(token != 258 && token != 0 && counter < 100);

	//while ((token == yylex()) != 0) {
//	while(!feof(yyin) {
//		token = yylex();
//		printf("Token: %d (%s)\n", token, yytext);	
//	}

	return 0;
}
*/
