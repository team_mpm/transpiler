%{
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "hass-declarations.h"
#include "stack.h"

extern int yylex();
extern int yyparse();
extern struct Stack* PREVIOUS_INDENTS;
extern void debug(char* state, char* rule, char* text);
extern FILE* yyin;
extern char* cp(const char*);
//extern char* concat(const char*, const char*);

void yyerror(char* s, ...);

struct ast* result;

%}

%union {
	int ival;
	float fval;
	char* sval;
	struct ast* ast;
}

%token T_EOF T_END T_DASH T_LSPARAN T_RSPARAN T_LCPARAN T_RCPARAN T_COMMA
%token<sval> T_ID T_STRING T_KEY
%token<ival> T_INT
%token<fval> T_REAL
%token T_SAME T_INDENT T_DEDENT

%type<ast>  wrapper 
%type<ast>  object
%type<ast>  objectitem
%type<ast>  identifier
%type<sval> identifierconcat
%type<ast>  list 
%type<ast>  listitem

%start wrapper

%%

wrapper: T_SAME object { debug("wrapper", "", ""); result = $2; $$ = $2; }
;

identifier: identifierconcat       { debug("identifier", "identifierconcat", $1); $$ = newstring($1); }
	  | T_INT                  { debug("identifier", "T_INT", ""); $$ = newinteger($1); }
	  | T_REAL                 { debug("identifier", "T_REAL", ""); $$ = newreal($1); }
	  | T_STRING               { debug("identifier", "T_STRING", $1); $$ = newstring($1); }
;

identifierconcat: T_ID identifierconcat  { $$ = concat($1, $2) /*newstr("id concat")*/; }
		| T_ID                   { $$ = cp($1); }
;

list: list listitem    { debug("list", "list listitem", ""); $$ = newast('B', $1, $2); }
    | listitem         { debug("list", "listitem", ""); $$ = $1; }
;

listitem: T_DASH identifier  { debug("listitem", "T_DASH identifier", ""); $$ = newsimplelist($2); }
	| T_DASH object      { debug("listitem", "T_DASH object", ""); $$ = newobjectlist($2); }
;

object: object T_SAME objectitem { debug("object", "object T_SAME objectitem", ""); $$ = newast('A', $1, $3); }
      | objectitem               { debug("object", "objectitem", ""); $$ = $1; }
;

objectitem: T_KEY identifier                      { debug("objectitem", "1", ""); $$ = newfieldobject($1, $2); /*$$ = newast('F', newobj('F', $1), $2);*/ }
	  | T_KEY T_INDENT list T_DEDENT          { debug("objectitem", "2", ""); $$ = newlistobject($1, $3); /*newast('L', newobj('L', $1), $3); */}
	  | T_KEY T_LSPARAN T_RSPARAN             { debug("objectitem", "3", ""); $$ = newelistobject($1); /*$$ = newobj('E', $1);*/ }
	  | T_KEY T_LCPARAN T_RCPARAN             { debug("objectitem", "4", ""); $$ = neweobjectobject($1); /*newobj('O', $1); */}
	  | T_KEY T_INDENT T_SAME object T_DEDENT { debug("objectitem", "5", ""); $$ = newnobject($1, $4); /*newast('N', newobj('N', $1), $4); */ }  
	  | T_KEY                                 { debug("objectitem", "5", ""); $$ = newsimpleobject($1); /*newobj('S', $1); */ }
;

%%

int main(argc, argv) int argc; char **argv; {
	PREVIOUS_INDENTS = createStack(20);
	if (argc > 1) {
		if (!(yyin = fopen(argv[1], "r"))) {
			perror(argv[1]);
			return 1;
		}
	}
	do {
		yyparse();
	} while(!feof(yyin));
	//printf("Parsing complete, building print result...\n");
	char* jsonresult = eval(result);
	//printf("Result:\n{%s}\n", jsonresult);
	printf("{%s}", jsonresult);
	return 0;
}

void yyerror(char* s, ...) {
	va_list ap;
	va_start(ap, s);
	printf("in yyerror\n");	
	fprintf(stderr, "%d: error: ", yylineno);
	vfprintf(stderr, s, ap);
	fprintf(stderr, "\n");
	exit(1);
}

char* evalConnector(struct ast* connector) {
	//printf("EvalConnector: %c\n", connector->nodetype);

	char* leftRes;
	char* rightRes;
	struct ast* left = connector->l;
	struct ast* right = connector->r;

	switch(connector->nodetype) {
		case 'A': ; // ConnectorObject
			leftRes = evalObject(left);
			rightRes = evalObject(right);
	
			return concat(leftRes, concat(", ", rightRes)); 
		case 'B': ; // ConnectorList
			leftRes = evalList(left);
			rightRes = evalList(right);
		
			return concat(leftRes, concat(", ", rightRes));
/*		case 'F': ; // FieldObject
			leftRes = evalObject(left);
			rightRes = evalIdentifier(right);
			
			return concat(leftRes, concat(": ", rightRes));
		case 'L': ; // ListObject
			leftRes = evalObject(left);
			if (right->nodetype == 'C') {
				rightRes = evalConnector(right);
			}	
			else {
				rightRes = evalObject(right);
			}
			return concat(leftRes, concat(": [", concat(rightRes, "]")));
		case 'N': ; // NObject
			leftRes = evalObject(left);
			if (right->nodetype == 'C') {
				rightRes = evalConnector(right);
			}
			else {
				rightRes = evalObject(right);
			}
			
			return concat(leftRes, concat(": {", concat(rightRes, "}")));
		*/	 
		default:
			break;
	}
	printf("Connector: TODO on %c\n", connector->nodetype);
	return "TODO";
}

char* evalIdentifier(struct ast* identifier) {
	//printf("EvalIdentifier: %c\n", identifier->nodetype);
	switch(identifier->nodetype) {
		case 'D': ; // Identifier
			return wrapInQuotes(((struct identifier *)identifier)->str);
		case 'S': ; // String	
			return wrapInQuotes(((struct string *)identifier)->str);
		case 'R': ; // Real
			double real = (((struct real *)identifier)->d);
			char* rbuff = malloc(10 * sizeof(int));
			sprintf(rbuff, "%f", real);
			return rbuff;
		case 'I': ; // Integer
			int integer = (((struct integer *)identifier)->i);
			char* ibuff = malloc(10 * sizeof(int));
			sprintf(ibuff, "%d", integer);
			return ibuff;
		default:
			break;
	}
	printf("Identifier: TODO on type %c\n", identifier->nodetype);
	exit(2);
}

char* evalObject(struct ast* object) {
	//printf("EvalObject: %c\n", object->nodetype);
	struct objectval *obval = ((struct objectval *)object);
	switch(object->nodetype) {
		case 'A': ; // ConnectorObject
		case 'B': ; // ConnectorList
			return evalConnector(object);

		case 'M': ; // SimpleObject
			return concat(wrapInQuotes((((struct simpleobject *)object)->str)), ": {}");
		case 'F': ; // FieldObject
			struct fieldobject *fo = (struct fieldobject *)object;
			return concat(wrapInQuotes(fo->str), concat(": ", evalIdentifier(fo->identifier)));
		case 'L': ; // ListObject
			struct listobject *lo = (struct listobject *)object;
			return concat(wrapInQuotes(lo->str), concat(": [", concat(evalList(lo->list), "]")));
		case 'N': ; // NObject
			struct nobject *no = (struct nobject *)object;
			return concat(wrapInQuotes(no->str), concat(": {", concat(evalObject(no->object), "}")));
		case 'E': ; // EListObject
			return concat(wrapInQuotes(((struct elistobject *)object)->str), ": []");
		case 'O': ; // EObjectObject
			return concat(wrapInQuotes(((struct eobjectobject *)object)->str), ": {}");
		default:
			break;
	}
	printf("Object: TODO on type %c\n", object->nodetype);
	exit(3);
}
char* evalList(struct ast* list) {
	//printf("EvalList: %c\n", list->nodetype);
	switch(list->nodetype) {
		case 'A': ; // ConnectorObject
		case 'B': ; // ConnectorList
			return evalConnector(list);

		case 'P': ; // SimpleList
			return evalIdentifier(((struct simplelist *)list)->identifier);
		case 'J': ; // ObjectList
			return concat("{", concat(evalObject(((struct objectlist *)list)->object), "}"));
		default:
			break;
	}
	printf("List: TODO on type %c\n", list->nodetype);
	exit(4);
}


char* eval(struct ast* ast) {
	//printf("Eval: %c\n", ast->nodetype);
	switch(ast->nodetype) {
		case 'A': ; // ConnectorObject
		case 'B': ; // ConnectorList
			return evalConnector(ast);
		case 'D': ; // Identifier
		case 'S': ; // String
		case 'I': ; // Integer
		case 'R': ; // Real
			return evalIdentifier(ast);
		case 'M': ; // SimpleObject
		case 'F': ; // FieldObject
		case 'L': ; // ListObject
		case 'N': ; // NObject
		case 'E': ; // EListObject
		case 'O': ; // EObjectObject
			return evalObject(ast);
		case 'P': ; // SimpleList
		case 'J': ; // ObjectList
			return evalList(ast);
		default:
			break;
	}
	printf("Eval: TODO on type %c\n", ast->nodetype);
	exit(5);
}
/*
char* concat(const char *s1, const char *s2) {
	char *result = malloc(strlen(s1)+strlen(s2) + 1); // +1 for the null-terminator
	// in real code you would check for erros in malloc here
	strcpy(result, s1);
	strcat(result, s2);
	return result;
}
*/
char* wrapInQuotes(const char *s) {
	char *result = malloc(strlen(s) + 3); // +1 for null, +2 for quotes
	strcpy(result, "\"");
	strcat(result, s);
	strcat(result, "\"");
	return result;
}
