%option noyywrap
%option yylineno

%x STRING

%{
#include <stdio.h>
#define YY_DECL int yylex()
#include "json2yaml.tab.h"

char *str = "";

int debugFlag = 0;
int lexFlag = 0;

char* concat(const char *s1, const char *s2) {
	char *result = malloc(strlen(s1)+strlen(s2)+1); // +1 for the null-terminator
	// TODO - in real code you would check for errors in malloc here

	strcpy(result, s1);
	strcat(result, s2);
	return result;
}

char* cp(const char* s) {
	char *result = malloc(strlen(s) + 1);
	strcpy(result, s);
	return result;
}

void debug(char* state, char* rule, char* text) {
	if (debugFlag) {
		printf("State: %s, Rule: %s Text: %s\n", state, rule, text);	
	}
}

void debugLexing(char* token, char* value) {
	if (lexFlag) {
		printf("Token %s (%s)\n", token, value);
	}
}

void trim(char* c) {
	memmove(c, c + 1, strlen(c) - 1);
	c[strlen(c) - 2] = '\0';
}

void keytrim(char* c) {
	int i;
	for (i = strlen(c) - 1; i > 0; i--) {
		
		if (c[i] == ' ' && c[i - 1] == ':') {
			memmove(c, c, i - 2);
			c[i - 1] = '\0';
			break;
		}
		if (c[i] == '\n' && c[i - 1] == ':') {
			memmove(c, c, i - 2);
			c[i - 1] = '\0';
			break;
		}		
	}
}

void newline() {
	yylineno++;
}

%}

DIGITS 		[0-9]+
REAL            [0-9]+\.[0-9]+
KEYCHAR         [a-zA-Z0-9_]
IDCHAR          [a-zA-Z0-9_/.]
QUOTED          [a-zA-Z0-9_/.: ]
WS              [ \t]
STRINGSINGLE	([-a-zA-Z0-9$_/:\"]|\\[^.])       
STRINGDOUBLE	([-a-zA-Z0-9$_/:\']|\\[^.])       

%%

<INITIAL>\n                                 { debug("INITIAL", "NEWLINE", ""); }
<INITIAL>"{"                                { debug("INITIAL", "START OBJECT", yytext); debugLexing("T_OBJECT_START", ""); return T_OBJECT_START; }
<INITIAL>"}"                                { debug("INITIAL", "END OBJECT", yytext); debugLexing("T_OBJECT_END", ""); return T_OBJECT_END; }
<INITIAL>"["                                { debug("INITIAL", "START ARRAY", yytext); debugLexing("T_ARRAY_START", ""); return T_ARRAY_START; }
<INITIAL>"]"                                { debug("INITIAL", "END ARRAY", yytext); debugLexing("T_ARRAY_END", ""); return T_ARRAY_END; }
<INITIAL>\"                                 { debug("INITIAL", "START STRING", yytext); str = ""; BEGIN STRING; }
<INITIAL>{WS}+                              { debug("INITIAL", "WHITESPACE", yytext); }
<INITIAL>-?{DIGITS}+                        { debug("INITIAL", "INTEGER", yytext); debugLexing("T_INT", yytext); yylval.ival = atoi(yytext); return T_INT; }
<INITIAL>-?{REAL}			    { debug("INITIAL", "REAL", yytext); debugLexing("T_REAL", yytext); yylval.fval = atof(yytext); return T_REAL; }
<INITIAL>"null"                             { debug("INITIAL", "NULL", yytext); debugLexing("T_NULL", ""); return T_NULL; }
<INITIAL>"false"                            { debug("INITIAL", "FALSE", yytext); debugLexing("T_FALSE", ""); return T_FALSE; }
<INITIAL>"true"                             { debug("INITIAL", "TRUE", yytext); debugLexing("T_TRUE", ""); return T_TRUE; }
<INITIAL>","                                { debug("INITIAL", "COMMA", yytext); debugLexing("T_COMMA", ""); return T_COMMA; }
<INITIAL>":"                                { debug("INITIAL", "COLON", yytext); debugLexing("T_COLON", ""); return T_COLON; }
<INITIAL>\"link\"                           { debug("INITIAL", "LINK", yytext); debugLexing("T_LINK", ""); return T_LINK; }

<INITIAL><<EOF>>                            { debug("INITIAL", "EOF", ""); return 0; }

<STRING>\"                        	    { debug("STRING", "QUOTE", yytext); yylval.sval = cp(str); debugLexing("T_STRING", str); BEGIN INITIAL; return T_STRING; }
<STRING>\\\"                       	    { debug("STRING", "ESCAPED", yytext); str = concat(str, cp(yytext)); }
<STRING>.                	            { /*debug("STRINGS", "CHAR", yytext);*/ str = concat(str, cp(yytext)); } 


%%
/*
YYSTYPE yylval;

#include <stdarg.h>

int main(int argc, char** av) {
	PREVIOUS_INDENTS = createStack(20);
	int token;
	yyin = fopen(av[1], "r");
	//printf("%d", yylex());
	//printf("%d", yylex());
	int counter = 0;
	do {
		counter++;
		token = yylex();
		printf("Token: %d (%s)\n", token, yytext);
	} while(token != 258 && token != 0 && counter < 100);

	//while ((token == yylex()) != 0) {
//	while(!feof(yyin) {
//		token = yylex();
//		printf("Token: %d (%s)\n", token, yytext);	
//	}

	return 0;
}
*/
