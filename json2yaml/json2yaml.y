%{
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include "hass-declarations.h"

extern int yylex();
extern int yyparse();
extern void debug(char* state, char* rule, char* text);
extern FILE* yyin;
extern struct Stack* PREVIOUS_INDENTS;
extern char* cp(const char*);

void yyerror(char* s, ...);

struct ast* result;

%}

%union {
	int ival;
	float fval;
	char* sval;
	struct ast* ast;
}

%token T_OBJECT_START T_OBJECT_END T_ARRAY_START T_ARRAY_END 
%token T_NULL T_FALSE T_TRUE T_LINK 
%token T_COMMA T_COLON
%token<sval> T_STRING 
%token<ival> T_INT
%token<fval> T_REAL

%type<ast> wrapper
%type<ast> object
%type<ast> members
%type<ast> pair
%type<ast> array
%type<ast> elements
%type<ast> value

%start wrapper

%%

wrapper: object         { debug("wrapper", "object", ""); result = $1; $$ = $1; }
       | array          { debug("wrapper", "array", ""); result = $1; $$ = $1; }
;

object: T_OBJECT_START T_OBJECT_END                         { debug("object", "EMPTY", "{}"); $$ = newdelegate('O', newempty()); }
      | T_OBJECT_START T_STRING T_COLON value T_OBJECT_END    { debug("object", "members", "{ ... }"); $$ = newdelegate('O', newpair(cp($2), $4)); }
      | T_OBJECT_START pair T_COMMA members T_OBJECT_END    { debug("object", "members", "{ ... }"); $$ = newdelegate('O', newast('M', $2, $4)); }
      | T_OBJECT_START T_LINK T_COLON T_STRING T_OBJECT_END { debug("object", "link", $4); $$ = newdelegate('O', newinclude($4)); }
;

members: pair                 { debug("members", "pair", ""); $$ = $1; }
       | pair T_COMMA members { debug("members", "pair", "members"); $$ = newast('M', $1, $3); }
;

pair: T_STRING T_COLON value { debug("pair", "string: value", ""); $$ = newpair(cp($1), $3); }
    | T_LINK T_COLON value   { debug("pair", "link: value", ""); $$ = newpair(cp("link"), $3); }
;

array: T_ARRAY_START T_ARRAY_END          { debug("array", "EMPTY", "[]"); $$ = newdelegate('A', newempty()); }
     | T_ARRAY_START elements T_ARRAY_END { debug("array", "elements", "[ ... ]"); $$ = newdelegate('A', $2); }
;

elements: value                  { debug("elements", "value", ""); $$ = $1; }
	| value T_COMMA elements { debug("elements", "value, members", ""); $$ = newast('E', $1, $3); }
;

value: T_STRING   { debug("value", "string", ""); $$ = newstring(cp($1)); }
     | T_LINK     { debug("value", "link", ""); $$ = newstring(cp("link")); }
     | T_INT      { debug("value", "int", ""); $$ = newinteger($1); }
     | T_REAL     { debug("value", "real", ""); $$ = newreal($1); }
     | object     { debug("value", "object", ""); $$ = $1; }
     | array      { debug("value", "array", ""); $$ = $1; }
     | T_TRUE     { debug("value", "true", ""); $$ = newboolean('T'); }
     | T_FALSE    { debug("value", "false", ""); $$= newboolean('F'); }
;

%%

int main(argc, argv) int argc; char **argv; {
/*	if (argc > 1) {
		if (!(yyin = fopen(argv[1], "r"))) {
			perror(argv[1]);
			return 1;
		}
	}
*/		
	do {
		yyparse();
	} while(!feof(yyin));
	//printf("Parsing complete, building print result...\n");
//	char* yamlresult = eval(result, 0, true);
	char* yamlresult = evalStart(result);
	//printf("Result:\n%s\n", yamlresult);
	printf("%s\n", yamlresult);
	return 0;
}

void yyerror(char* s, ...) {
	va_list ap;
	va_start(ap, s);
	printf("in yyerror\n");	
	fprintf(stderr, "%d: error: ", yylineno);
	vfprintf(stderr, s, ap);
	fprintf(stderr, "\n");
	exit(1);
}

char* makeIndentStr(int indent) {
	char *str = malloc(indent + 1); //malloc(sizeof(char) * (indent + 1)); // +1 for null terminator ?
	memset(str, ' ', indent);
	str[indent] = '\0';
	return str;
}

char* evalStart(struct ast* ast) {
	switch(ast->nodetype) {
		case 'O': ; // Object
			return evalObject(ast, -2); // Counteract the outer object
		case 'A': ; // Array
			return evalArray(ast, 0);
		default:
			break;
	}
	printf("Invalid start type: %c\n", ast->nodetype);
	exit(-1);
}

char* evalObject(struct ast* ast, int indent) {
	struct delegate* o = ((struct delegate*)ast);
	struct ast* d = o->delegate;
	if (d->nodetype == 'V') {
		// Empty object
		return "{}";
	}
	else if (d->nodetype == 'C') {
		return concat("!include ", ((struct string*)d)->str);
	}
	else {
		// Members
		return concat("\n", evalMembers(d, indent + 2));
	}
}

char* evalMembers(struct ast* ast, int indent) {
//	char* indentStr = makeIndentStr(indent);

	if (ast->nodetype == 'M') {
		// Pair, members
		struct ast* l = ast->l;
		struct ast* r = ast->r;

		char* pairPart = evalPair(l, indent);
		return concat(pairPart, concat("\n", evalMembers(r, indent)));
	}
	else {
		// Pair
		return evalPair(ast, indent);
	}
}

char* evalPair(struct ast* ast, int indent) {
	char* indentStr = makeIndentStr(indent);
	struct pair* p = ((struct pair*)ast);
	char* str = p->str;
	struct ast* value = p->value;
	return concat(indentStr, concat(str, concat(": ", evalValue(value, indent))));
}

char* evalArray(struct ast* ast, int indent) {
	struct delegate* a = ((struct delegate*)ast);
	struct ast* d = a->delegate;

	if (d->nodetype == 'V') {
		// Void	
		return "[]";
		
	}
	else {
		// Elements
		return concat("\n", evalElements(d, indent + 2));
	}
}

char* evalElements(struct ast* ast, int indent) {
	char* indentStr = concat(makeIndentStr(indent), "- ");
	if (ast->nodetype == 'E') {
		// Value, elements
		struct ast* l = ast->l;
		struct ast* r = ast->r;

		char* valuePart = concat(indentStr, evalValue(l, indent));
		
		return  concat(valuePart, concat("\n", evalElements(r, indent)));
	}
	else {
		// Value
		return concat(indentStr, evalValue(ast, indent));
	}
}

char* evalValue(struct ast* ast, int indent) {
	switch(ast->nodetype) {
		case 'S': ; // String
			char* strStr = ((struct string*)ast)->str;
			return wrapInQuotes(strStr);
		case 'I': ; // Integer
			char* intStr = int2str(((struct integer*)ast)->i);
			return intStr;
		case 'R': ; // Real
			char* realStr = real2str(((struct real*)ast)->d);
			return realStr;
		case 'O': ; // Object
			return evalObject(ast, indent + 2);
		case 'A': ; // Array
			return evalArray(ast, indent + 2);
		case 'T': ; // True
			return "true";
		case 'F': ; // False
			return "false";
		case 'V': ; // Null (Void)
			return "";
		default:
			break;
	}
}
/*
char* eval(struct ast* ast, int indent, bool insertIndent) {
	// printf("Eval: %c\n", ast->nodetype);
	char* indentStr = makeIndentStr(indent);
	switch(ast->nodetype) {
		case 'M': ; // Members
			char* left = eval(ast->l, indent, true);
			char* right = eval(ast->r, indent, true);
			return concat(left, concat("\n", right));
		case 'E': ; // Elements	
			return evalElements(ast, indent);
//			left = eval(ast->l, indent);
//			right = eval(ast->r, indent);
//			if 

//			return concat(concat("- ", left), concat("\n", right));
		case 'O': ; // Object
			struct ast* delObj = ((struct delegate*)ast)->delegate;
			return eval(delObj, indent, true);
		case 'A': ; // Array
			struct ast* delList = ((struct delegate*)ast)->delegate;
			return evalElements(delList, indent);
		case 'S': ; // String
			char* str = ((struct string*)ast)->str;
			return wrapInQuotes(str);
		case 'I': ; // Integer
			int integer = ((struct integer*)ast)->i;
			char* iPointer = malloc(21 * sizeof(char));
			sprintf(iPointer, "%d", integer);
			return iPointer;
		case 'R': ; // Real
			double real = ((struct real*)ast)->d;
			char* dPointer = malloc(21 * sizeof(char));
			sprintf(dPointer, "%f", real);
			return dPointer;
		case 'P': ; // Pair
			struct pair* p = (struct pair*)ast;
			return evalPair(p, indent, insertIndent);	
		case 'T': ; // True
			return "true";
		case 'F': ; // False
			return "false";
		case 'V': ; // Empty (Void)
			return "";
		default:
			break;
	}
	printf("TODO - Eval with %c\n", ast->nodetype);
	exit(5);
}

char* evalElements(struct ast* elements, int indent) {
	char* indentStr = concat(makeIndentStr(indent), "- ");;	

	switch(elements->nodetype) {
		case 'E':
			struct ast* l = elements->l;
			struct ast* r = elements->r;
			return concat(indentStr, concat(evalElements(l, indent), "\n", evalElements(r, indent)));
		case 'S':
			return concat(indentStr, wrapInQuotes(((struct string*)elements)->str));
		case 'I'
			return concat(indentStr, 
	}
	

	if (r->nodetype == 'E') {
		return concat(indentStr, concat("- ", concat(eval(l, indent, false), concat("\n", eval(r, indent, false)))));
	}
	else {
		return concat(indentStr, concat("- ", eval(r, indent, false)));
	}	
}

char* evalPair(struct pair* pair, int indent, bool insertIndent) {
	struct ast* substruct = pair->value;
	char* str = pair->str;

	char* indentStr;
	if (insertIndent) {
		indentStr = makeIndentStr(indent);
	}
	else {
		indentStr = "";
	}
	//printf("----- %s -----\n", str);
	switch(substruct->nodetype) {
		case 'S': ; // String
			return concat(indentStr, concat(str, concat(": \"", concat(((struct string*)substruct)->str,"\""))));
		case 'I': ; // Integer
			char* intStr = int2str(((struct integer*)substruct)->i);
			return concat(indentStr, concat(str, concat(": ", intStr)));
		case 'R': ; // Real
			double real = ((struct real*)substruct)->d;
			char* dPointer = malloc(21 * sizeof(char));
			sprintf(dPointer, "%f", real);
			return concat(indentStr, concat(str, concat(": ", dPointer)));
		case 'O': ; // Object
			return concat(indentStr, concat(str, concat(":\n", concat(eval(substruct, indent + 2, insertIndent), "\n"))));
		case 'A': ; // Array
			return concat(indentStr, concat(str, concat(":\n", concat(eval(substruct, indent + 2, insertIndent), "\n"))));
		case 'T': ; // True
			return concat(indentStr, concat(str, ": true\n"));
		case 'F': ; // False
			return concat(indentStr, concat(str, ": false\n"));
		case 'V': ; // Empty, Void, Null
			return concat(indentStr, concat(str, ": "));
		default:
			break;
	}
	printf("TODO - EvalPair with %c\n", substruct->nodetype);
	exit(1);
}
*/
char* int2str(int i) {
	char* pointer = malloc(21 * sizeof(char));
	sprintf(pointer, "%d", i);
	return pointer;
}

char* real2str(double d) {
	char* pointer = malloc(21 * sizeof(char));
	sprintf(pointer, "%f", d);
	return pointer;
}

char* wrapInQuotes(const char *s) {
	char *result = malloc(strlen(s) + 3); // +1 for null, +2 for quotes
	strcpy(result, "\"");
	strcat(result, s);
	strcat(result, "\"");
	return result;
}
