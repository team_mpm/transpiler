/*
 *Declarations for HASS
 */

/* Interface to the lexer */
extern int yylineno;
void yyerror(char *s, ...);


struct ast {
	int nodetype; // M = Members, E = elements
	struct ast* l;
	struct ast* r;
};

struct ast* newast(int type, struct ast *l, struct ast *r) {
	struct ast *a = malloc(sizeof(struct ast));
	a->nodetype = type;
	a->l = l;
	a->r = r;
	return a;	
}

struct delegate {
	int nodetype; // O = Object, A = Array
	struct ast* delegate;
};

struct ast* newdelegate(int type, struct ast *d) {
	struct delegate *a = malloc(sizeof(struct delegate));
	a->nodetype = type;
	a->delegate = d;
	return (struct ast*)a;
}	

struct string {
	int nodetype; // S = String, C = Include
	char* str;
};

struct ast* newstring(char* str) {
	struct string *a = malloc(sizeof(struct string));
	a->nodetype = 'S';
	a->str = str;
	return (struct ast*)a;
}

struct ast* newinclude(char* str) {
	struct string *a = malloc(sizeof(struct string));
	a->nodetype = 'C';
	a->str = str;
	return (struct ast*)a;
}

struct integer {
	int nodetype; // I
	int i;
};

struct ast* newinteger(int i) {
	struct integer *a = malloc(sizeof(struct integer));
	a->nodetype = 'I';
	a->i = i;
	return (struct ast*)a;
}

struct real {
	int nodetype; // R
	double d;
};

struct ast* newreal(double d) {
	struct real *a = malloc(sizeof(struct real));
	a->nodetype = 'R';
	a->d = d;
	return (struct ast*)a;
}

struct pair {
	int nodetype; // P
	char* str;
	struct ast* value;
};

struct ast* newpair(char* s, struct ast* v) {
	struct pair *a = malloc(sizeof(struct pair));
	a->nodetype = 'P';
	a->str = s;
	a->value = v;
	return (struct ast*)a;
}

struct boolean {
	int nodetype; // T = True, F = False
};

struct ast* newboolean(int nodetype) {
	struct boolean *a = malloc(sizeof(struct boolean));
	a->nodetype = nodetype;
	return (struct ast*)a;
}

struct empty {
	int nodetype; // V (void)
};

struct ast* newempty() {
	struct empty *a = malloc(sizeof(struct empty));
	a->nodetype = 'V';
	return (struct ast*)a;
}

/* Nodes in the abstract syntax tree */
/*
struct ast {
	int nodetype; // type C for connector 
	int subtype; 
	// subtype general: A = ConnectorObject, B = ConnectorList 
	struct ast *l;
	struct ast *r;
	// subtypes:
	// F = FieldObject, L = ListObject, N = NObject,
	// O = ObjectList
	//
};

struct identifierval {
	int nodetype; // type I for identifier 
	int subtype;
	char* string; // subtype S for string, D for identifier, C for inClude 
	double dnumber; // substype R for real 
	int inumber; // subtype I for integer 
};

struct objectval {
	int nodetype; // type O for object 
	int subtype;
	char* str;
	// subtypes: 
	// S = SimpleObject, F = FieldObject, L = ListObject,
	// N = NObject, E = ElistObject, O = EObjectObject,
	//
	// X = ObjectList
	//
};	

struct listval {
	int nodetype; // type L for list 
	int subtype;
	// subtypes:
	// S = SimpleList, O = ObjectList 
	//
};

struct unkval {
	int nodetype; // type U for unknown 
};
*/

// Helpers 
char* concat(const char*, const char*);
char* wrapInQuotes(const char*);
char* int2str(int);
char* real2str(double);

// Build an AST 
/*
struct ast *newast(int subtype, struct ast *l, struct ast *r);
struct ast *newstr(char* str);
struct ast *newreal(double d);
struct ast *newint(int i);
struct ast *newlst(int subtype);
struct ast *newobj(int subtype, char* str);
*/

// Evaluate an AST 
//char* eval(struct ast*, int indent, bool insertIndent);
//char* evalPair(struct pair*, int indent, bool insertIndent);
//char* evalElements(struct ast*, int indent);
//char* eval(struct ast*);
//char* evalIdentifier(struct ast*);
//char* evalObject(struct ast*);
//char* evalList(struct ast*);
char* evalStart(struct ast*);
char* evalObject(struct ast*, int);
char* evalMembers(struct ast*, int);
char* evalPair(struct ast*, int);
char* evalArray(struct ast*, int);
char* evalElements(struct ast*, int);
char* evalValue(struct ast*, int);


// Delete and free an AST 
void treefree(struct ast*);

