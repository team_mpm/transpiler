from flask import Flask, request
from flask_cors import CORS
from subprocess import run, PIPE, call
import json
import requests
import homeassistant.remote as remote

api = remote.API('127.0.0.1', '')

app = Flask(__name__)
CORS(app)

hassHome = "/home/homeassistant/.homeassistant/"

@app.route("/load")
def load():
        pathConfig = "configuration.yaml"
        pathA = hassHome + "specialized-lexer"
        includeRes = run([pathA, pathConfig, hassHome], check=True, stdout=PIPE).stdout
        parseRes = run(hassHome + "yaml2json", input=includeRes, stdout=PIPE, check=True).stdout.decode("utf-8")
        return parseRes;

@app.route("/save", methods = ['POST'])
def save():
    data = json.loads(request.data.decode('utf8'))
    files = []
    for filename in data:
        files.append(filename);
        call(['mv', hassHome + filename, hassHome + "_" + filename])
        content = data[filename]
        inp = json.dumps(content).encode("utf-8")
        fileRes = run(hassHome + "json2yaml", input=inp, stdout=PIPE, check=True).stdout.decode("utf-8")
        with open(hassHome + filename, "w") as f:
            f.write(fileRes)

    r = requests.post("http://raspberrypi.local:8123/api/config/core/check_config")
    resp = r.json()

    if resp['errors'] is not None:
        for filename in files:
            call(['mv', hassHome + "_" + filename, hassHome + filename])
        for filename in files:
            call(['rm', hassHome + "_" + filename]) 
        errors = resp['errors']
        
        return json.dumps({'success': False, "text": errors}), 400, {'ContentType': 'application/json'}
    else:
        for filename in files:
            call(['rm', hassHome + "_" + filename]) 

        reload_configs()
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}

def reload_configs():
    remote.call_service(api, 'homeassistant', 'reload_core_config')
    remote.call_service(api, 'automation', 'reload')
    remote.call_service(api, 'group', 'reload')
    remote.call_service(api, 'script', 'reload')

@app.route("/custom_groupings", methods = ['GET'])
def custom_groupings_get():
    with open(hassHome + "custom_groupings.json", "r") as f:
        return f.read(), 200, {'ContentType': 'application/json'}


@app.route("/custom_groupings", methods = ['PUT'])
def custom_grouping_put():
    data = request.data.decode('utf8')
    with open(hassHome + "custom_groupings.json", "w") as f:
        f.write(data)
    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}






if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
